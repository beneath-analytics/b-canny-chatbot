## happy greet
  - utter_greet
* greet

## happy path
- utter_greet
* greet
  - utter_greet
* intro
  - utter_intro
* model
  - utter_model
* ram
  - utter_ram
* display
  - utter_display
* camera
  - utter_camera
* processor
  - utter_processor
* colour
  - utter_colour
* storage
  - utter_storage
* battery
  - utter_battery
* goodbye
  - utter_goodbye



# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/core/actions/#custom-actions/

# <script type="application/javascript" crossorigin="anonymous" src="https://securegw-stage.paytm.in/merchantpgpui/checkoutjs/merchants/VaSQXz51142210543876.js" onload="onScriptLoad();"> </script>

# This is a simple example for a custom action which utters "Hello World!"
'''
from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from payment import Payment
#
class ActionHelloWorld(Action):
#
    def name(self) -> Text: #returns Text
        return "action_payment"

    def run(self, dispatcher: CollectingDispatcher, tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
      final=Payment()
      dispatcher.utter_template("utter_pay", tracker, final)
      return []
#https://xlog.x-hub.io/build-your-chatbot-with-rasa-and-slack-from-training-to-deploying/
#https://rasa.com/docs/rasa/api/rasa-sdk/
#https://www.cedextech.com/blog/rasa-chatbot/

import pymongo
# url="http://localhost:3000/api"
client = pymongo.MongoClient("localhost", 27017)
db=client.sample
'''
from rasa_core.trackers import DialogueStateTracker 

tracker1=DialogueStateTracker(sender_id,domain.slots) 
events=db.retrieve(sender_id).as_dialogue().event

for event in events:
    print(event.as_story_string())
tracker1.update(event)


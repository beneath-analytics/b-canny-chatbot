## intent:greet
- hey
- hello
- hi
- good morning
- good evening
- hey there
## intent: intro
- I'm good
- who are you?
- your name?
- chatbot's name is?
- name is?
- name pls?
- name?
## intent: model
- What is model
- Which is model
- What model
- How much is model
- how much model
- which model
## intent: colour
- What is colour
- Which is colour
- What colour
- How much is colour
- how much colour
- which colour
## intent: storage
- What is storage
- Which is storage
- What storage
- How much is storage
- how much storage
- which storage
## intent: ram
- What is ram
- Which is ram
- What ram
- How much is ram
- how much ram
- which ram
## intent: display
- What is display
- Which is display
- What display
- How much is display
- how much display
- which display
## intent: camera
- What is camera
- Which is camera
- What camera
- How much is camera
- how much camera
- which camera
## intent: battery
- What is battery
- Which is battery
- What battery
- How much is battery
- how much battery
- which battery
## intent: processor 
- What is processor 
- Which is processor 
- What processor 
- How much is processor 
- how much processor 
- which processor 
## intent:goodbye
- bye
- goodbye
- see you around
- see you later

